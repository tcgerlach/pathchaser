package com.talixa.pathchaser.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Stack;

import javax.swing.JPanel;

import com.talixa.pathchaser.ai.PathFinder;
import com.talixa.pathchaser.ai.graph.Graph;
import com.talixa.pathchaser.ai.graph.Node;

@SuppressWarnings("serial")
public class MazePanel extends JPanel {

	//800x600 = 8 cols x 6 rows where each block is 100x100
	private static final int MAP_HEIGHT = 600;
	private static final int MAP_WIDTH = 800;
	private static final int BLOCK_WIDTH = 50;
	private static final int BLOCK_HEIGHT = 50;
	private static final int ROW_COUNT = MAP_HEIGHT/BLOCK_HEIGHT;
	private static final int COL_COUNT = MAP_WIDTH/BLOCK_WIDTH;
			
	private static final String[] MAZE = 
		{
			"0101111111111111",
			"0101110010000001",
			"0000011000110101",
			"1011011101110101",
			"1001011111110111",
			"1001000000000001",
			"1111110111001011",
			"1000110101001001",
			"1010110101011011",
			"1010000101000011",
			"1010110001010001",
			"1111111111111101",
		};
	
	// graph of above maze
	private Graph graph;	
	private Stack<Node> path;
	
	// display data
	private Stack<Node> displayPath;
	private Node currentDisplayNode;
	private long lastUpdate = 0;
	private long lastChanged = 0;
	private static final long DISPLAY_TIME = 250;
	
	private int startX = 0;
	private int startY = 0;
	private int endX = COL_COUNT-2;
	private int endY = ROW_COUNT-1;
	
	public MazePanel() {
		graph = new Graph(COL_COUNT,ROW_COUNT,MAZE);
		Node start = graph.getNode(startX,startY);
		Node end = graph.getNode(endX,endY);
		path = PathFinder.findShortestPath(start, end);
				
		displayPath = new Stack<Node>();
		displayPath.addAll(path);
		lastUpdate = System.currentTimeMillis();
		lastChanged = lastUpdate;
		currentDisplayNode = displayPath.pop();
		
		this.addMouseListener(new MouseListener() {			
			public void mouseReleased(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}			
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {		
				// get grid coordinates
				int x = e.getX() / BLOCK_WIDTH;
				int y = e.getY() / BLOCK_HEIGHT;
				
				if (e.getButton() == MouseEvent.BUTTON1) {					
					goToNode(x, y);
				} else if (e.getButton() == MouseEvent.BUTTON3) {
					Node n = graph.getNode(x, y);
					n.setBlocked(!n.isBlocked());
					goToNode(startX,startY);
				}
			}
		});
	}
	
	@Override
	public void paint(Graphics g) {		
		super.paint(g);
		
		// update step of path to display
		lastUpdate = System.currentTimeMillis();
		if (lastUpdate - lastChanged > DISPLAY_TIME) {
			lastChanged = lastUpdate;
			// changing the maze can cause unexpected consequences...
			if (!displayPath.isEmpty()) {
				currentDisplayNode = displayPath.pop();
			}
			if (displayPath.isEmpty()) {
				if (currentDisplayNode.getX() == startX && currentDisplayNode.getY() == startY) {
					goToNode(endX,endY);
				} else {
					goToNode(startX,startY);
				}
			}
		}
		
		g.setColor(Color.BLACK);
		// draw maze and circle displaying progress
		for(int row = 0; row < ROW_COUNT; ++row) {
			for(int col = 0; col < COL_COUNT; ++col) {
				int topLeftX = col*BLOCK_WIDTH;
				int topLeftY = row*BLOCK_HEIGHT;		
				// we now have the coordinates - is this block to be darkened?
				if (graph.getNode(col, row).isBlocked()) {
					g.fillRect(topLeftX,topLeftY,BLOCK_WIDTH,BLOCK_HEIGHT);
				} else if (currentDisplayNode.getX() == col && currentDisplayNode.getY() == row) {
					g.setColor(Color.RED);
					g.fillOval(topLeftX,topLeftY,BLOCK_WIDTH,BLOCK_HEIGHT);
					g.setColor(Color.BLACK);		
				}
			}
		}
	}
	
	public void goToNode(int x, int y) {
		if (!graph.getNode(x, y).isBlocked()) {
			graph.reset();
			path = PathFinder.findShortestPath(currentDisplayNode, graph.getNode(x,y));
			displayPath = new Stack<Node>();
			displayPath.addAll(path);			
			currentDisplayNode = displayPath.pop();			
		}
	}
}
