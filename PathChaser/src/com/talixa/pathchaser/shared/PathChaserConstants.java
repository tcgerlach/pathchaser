package com.talixa.pathchaser.shared;

public class PathChaserConstants {
	public static final String VERSION = "1.0";
	
	public static final String TITLE_MAIN = "Path Chaser";	
	public static final String TITLE_ABOUT = "About " + TITLE_MAIN;
	
	public static final int APP_WIDTH = 815;
	public static final int APP_HEIGHT = 665;
	
	public static final int BORDER = 10;
	public static final int BORDER_SMALL = 5;
	
	public static final String LABEL_OK = "Ok";	
	
	public static final String MENU_FILE = "File";	
	public static final String MENU_EXIT = "Exit";
	public static final String MENU_HELP = "Help";
	public static final String MENU_ABOUT = "About";
	
	public static final String ICON = "res/icon.jpg";
}
