package com.talixa.pathchaser;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import com.talixa.pathchaser.frames.FrameAbout;
import com.talixa.pathchaser.listeners.DefaultWindowListener;
import com.talixa.pathchaser.listeners.ExitActionListener;
import com.talixa.pathchaser.shared.IconHelper;
import com.talixa.pathchaser.shared.PathChaserConstants;
import com.talixa.pathchaser.widgets.MazePanel;

public class PathChaser {

	private static JFrame frame;
	private static MazePanel maze;
	
	private static void createAndShowGUI() {
		frame = new JFrame(PathChaserConstants.TITLE_MAIN);
		
		// set close functionality 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		frame.addWindowListener(new DefaultWindowListener());
		
		maze = new MazePanel();
		frame.add(maze);
		
		// set icon
		IconHelper.setIcon(frame);	
													
		// add menus
		addMenus();			
		
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(PathChaserConstants.APP_WIDTH,PathChaserConstants.APP_HEIGHT));
		int left = (screenSize.width/2) - (PathChaserConstants.APP_WIDTH/2);
		int top  = (screenSize.height/2) - (PathChaserConstants.APP_HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);
		
		Thread animationThread = new Thread() {
			@Override
			public void run() {			
				super.run();
				while (true) {
					maze.repaint();
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// don't care
					}
				}				
			}
		};
		animationThread.start();
	}
			
	private static void addMenus() {
		// Setup file menu
		JMenu fileMenu = new JMenu(PathChaserConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);
			
		JMenuItem exitMenuItem = new JMenuItem(PathChaserConstants.MENU_EXIT);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new ExitActionListener(frame));
		fileMenu.add(exitMenuItem);							
				
		//*******************************************************************************
		// Setup help menu
		JMenu helpMenu = new JMenu(PathChaserConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		JMenuItem aboutMenuItem = new JMenuItem(PathChaserConstants.MENU_ABOUT);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(frame);				
			}						
		});
		helpMenu.add(aboutMenuItem);

		//*******************************************************************************
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);			
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}
		
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {			
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
