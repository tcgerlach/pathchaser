package com.talixa.pathchaser.ai.graph;

public class Node implements Comparable<Node> {
	
	private int x;
	private int y;
	private boolean blocked;
	private boolean visited;
	private int cost = 9999;
	private boolean path = false;	// set by search algorithm when path found
	private Node parent;			// set by search algorithm when path found
	private Node[] neighbors;		// numbered top left to bottom right
	
	public Node(int x, int y, boolean blocked) {
		this.x = x;
		this.y = y;
		this.blocked = blocked;
		this.neighbors = new Node[8];
	}
	
	public boolean isPath() {
		return path;
	}
	
	public void addToPath() {
		this.path = true;
	}
	
	public void setNeighbor(int position, Node node) {
		this.neighbors[position] = node;
	}

	public Node getNeighbor(int position) {
		return this.neighbors[position];
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}
	
	public int getCost() {
		return cost;
	}
	
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	public String toString() {
		return blocked ? "1" : "0";
	}

	@Override
	public int compareTo(Node other) {
		return this.cost - other.cost;
	}
}
