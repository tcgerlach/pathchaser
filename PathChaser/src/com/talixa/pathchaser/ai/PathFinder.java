package com.talixa.pathchaser.ai;

import java.util.PriorityQueue;
import java.util.Stack;

import com.talixa.pathchaser.ai.graph.Node;

public class PathFinder {
	// this is using Dijkstra's algorithm
	public static Stack<Node> findShortestPath(Node start, Node end) {
		Stack<Node> path = new Stack<Node>();
		PriorityQueue<Node> open = new PriorityQueue<Node>();
		Node n, child;
		start.setParent(null);
		start.setCost(0);
		open.add(start);
		while (!open.isEmpty()) {
			n = open.remove();
			n.setVisited(true);
			if (n == end) {
				// build return path
				path.add(n);
				n = n.getParent();
				while (n != null) {
					path.push(n);
					n = n.getParent();
				}
				return path;
			}
			for(int i = 0; i < 8; ++i) {
				child = n.getNeighbor(i);
				if (child == null) {
					continue;					
				}				
				if (child.isVisited()) {
					continue;
				}
				int newCost = n.getCost() + getCost(n,child,i);
				if (child.getCost() <= newCost) {
					continue;
				} else {
					child.setParent(n);
					child.setCost(newCost);
					if (open.contains(child)) {
						open.remove(child);
						open.add(child);
					} else {
						open.add(child);
					}
				}
			}
		}
		return null;
	}
	
	private static final int N = 1;
	private static final int W = 3;
	private static final int E = 4;
	private static final int S = 6;
	
	private static int getCost(Node n1, Node n2,int neighborId) {
		if (n2.isBlocked()) {
			return Integer.MAX_VALUE;
		} else {
			// cheapest neighbors are u/d/l/r 
			if (neighborId == N || neighborId == S || neighborId == E || neighborId == W) {
				return 1;
			} else {
				return 5;
			}
		}
	}
}
